#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH --job-name=cpgenome_assembly
#SBATCH --mem 30G
#SBATCH --cpus-per-task=10
#SBATCH -t 11-0
#SBATCH --out=cpgenome_assembly

####################### short reads #############################


##### install and load getorganelle Modules using anaconda #####

module add anaconda

# activate anaconda environment. Change the grasstool to your specified conda evironment name.
source activate grasstool

# change directory.
cd /CHANGE/TO/THE/ILLUMINA_RESULT/PATH/

# step1 of getorganelle download all datasets
get_organelle_config.py -a all &&

# step2 of getorganelle to generate the cpgenome.
#### change the path of R1.fastq and R2.fastq data accordingly. Please check the Data_links.txt file.
get_organelle_from_reads.py -1 /pine/scr/w/z/wzhou10/PASTOS23/cpgenome/filter_illumina/P23_filter_R1.fastq -2 /pine/scr/w/z/wzhou10/PASTOS23/cpgenome/filter_illumina/P23_filter_R2.fastq -o P23_cp -R 15 -k 21,39,45,65,85,105 -F embplant_pt -t 10


####################### long reads #############################
# use flye for long filtered long reads data. Optional. Use cpGAUL instead.
# flye --nano-raw filter_new_ONT_1k.fastq --genome-size 0.16m --out-dir ./flye_ONT_raw_output --threads 10

# change directory.
cd /CHANGE/TO/THE/ONT_RESULT/PATH/

##### modules needs to be loaded #####
# 1. minimap2
# 2. seqkit
# 3. assembly-stats
# 4. seqtk
# 5. flye

### running cpGAUL for Long reads data
### -l is followed by the longreads input. Please check the Data_links.txt file.
### -r is followed by the reference input. Please check the Data_links.txt file.
./cpGAUL.sh -l corrected.fasta -r 8_juncus_ref.fasta -t 6
